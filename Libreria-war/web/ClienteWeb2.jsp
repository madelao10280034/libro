

<%@page contentType="text/html" pageEncoding="UTF-8" import="entidad.*, stateless.*, java.math.BigDecimal, javax.naming.*, java.util.*"%>
<%!
    private LibroBeanRemote librocat = null;
    String s1, s2, s3, s0;
    Collection list;
    Libro libro;
    public void jspInit() {
        try {
            InitialContext context = new InitialContext();
            librocat = (LibroBeanRemote) context.lookup(LibroBeanRemote.class.getName());
            System.out.println("Cargando Catalogo Bean" + librocat);
        } catch (Exception ex) {
            System.out.println("error " + ex.getMessage());
        }
    }

    public void jspDestroy() {
        librocat = null;
    }
%>
<%
    try {
        s0 = request.getParameter("id");
        s1 = request.getParameter("t1");
        s2 = request.getParameter("aut");
        s3 = request.getParameter("precio");
        if (s1 != null && s2 != null && s2 != null && s0 != null) {
            Double precio = new Double(s3);
            BigDecimal b = new BigDecimal(precio);
            int ids=Integer.parseInt(s0);
            librocat.actualizarlibro(ids, s1, s2, b);
            System.out.println("Libro Actualizado");
        }
%>
<title>Editar registro de Libro</title>
<center>
<h1>Registro de Libros</h1>
<hr>
<p>Registro Actualizado</p>
<table border="1" width="500">
    <tr align="center">
        <td>ID:</td>
        <td>Titulo:</td>
        <td>Autor:</td>
        <td>Precio:</td>
    </tr>
<%
    list = librocat.getLibro();        
    for (Iterator iter = list.iterator(); iter.hasNext();) {
        Libro elemento = (Libro) iter.next();
%>
    <tr>
        <td align="center"><%=elemento.getId()%></td>
        <td><%=elemento.getTitulo()%></td>
        <td><%=elemento.getAutor()%></td>
        <td align="center"><%=elemento.getPrecio()%></td>
    </tr>
<%
    }
    response.flushBuffer();
%>
</table>
<p><a href="buscaid.jsp">Editar registro</a></p>
<p><a href="index.html">Inicio</a></p>
</center>     
<%
    } catch (Exception ex) {
        ex.printStackTrace();
    }
%>